package com.epam.rd.task2;

import java.util.Arrays;

public class CustomNodeList {
    private Node head;
    private Node tail;
    private int size;

    public CustomNodeList() {
        this.head = null;
        this.tail = null;
    }

    static class Node {

        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void add(int value) {
        Node item = new Node(value);

        if (!isEmpty()) {
            tail.setNext(item);
        } else {
            head = item;
        }
        tail = item;
        size++;
    }

    public void insert(int value, int index) {
        if (index > size) {
            throw new IndexOutOfBoundsException(String.format("Index greater than allowed: %d", size));
        }
        Node current = head;
        Node itemToInsert = new Node(value);
        int count = 0;
        if (index == size) {
            add(value);
        } else if (index == 0) {
            itemToInsert.setNext(head);
            head = itemToInsert;
            size++;
        } else {
            while (current != tail) {
                if (count + 1 == index) {
                    itemToInsert.setNext(current.getNext());
                    current.setNext(itemToInsert);
                    size++;
                }
                count++;
                current = current.getNext();
            }
        }
    }

    public void delete(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException(String.format("Index greater than allowed: %d", size - 1));
        }
        if (index == 0) {
            head = head.getNext();
            size--;
        } else if (index == size - 1) {
            pop();
        } else {
            Node current = head;
            int count = 0;
            while (current != tail) {
                if (index == count + 1) {
                    Node itemToDelete = current.getNext();
                    current.setNext(itemToDelete.getNext());
                    size--;
                }
                current = current.getNext();
                count++;
            }
        }
    }

    public void pop() {
        Node current = head;
        while (current != tail) {
            if (current.getNext() == tail) {
                current.setNext(null);
                tail = current;
                size--;
            }
            current = current.getNext();
        }
    }
    public void replace(int index, int newValue) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException(String.format("Index greater than allowed: %d", size));
        }
        if (index == 0) {
            head.setValue(newValue);
        }else {
            Node current = head.getNext();
            int count = 1;
            while (current != tail) {
                if (index == count) {
                    current.setValue(newValue);
                }
                current = current.getNext();
                count++;
            }
            if (index == count) {
                current.setValue(newValue);
            }
        }
    }

    private int[] getValues() {
        int[] resultArray = new int[size];
        Node current = head;
        int index = 0;
        while (current != tail) {
            resultArray[index] = current.getValue();
            current = current.getNext();
            index++;
        }
        resultArray[index] = current.getValue();
        return resultArray;
    }

    @Override
    public String toString() {
        return Arrays.toString(getValues());
    }
}
