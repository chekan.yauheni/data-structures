package com.epam.rd.task1;

import java.util.Arrays;

public class CustomArrayList {
    private int[] items;
    private int size;

    public CustomArrayList() {
        items = new int[size + 1];
        size = 0;
    }

    public void add(int value) {
        items = Arrays.copyOf(this.items, size + 1);
        items[size] = value;
        size++;
    }

    public void insert(int value, int index) {
        if (index != 0 && index > size - 1) {
            throw new IndexOutOfBoundsException(String.format("Index greater than allowed: %d", size - 1));
        }
        if (size == 0) {
            add(value);
        } else {
            int[] tempArray = new int[size + 1];
            System.arraycopy(items, 0, tempArray, 0, index);
            System.arraycopy(items, index, tempArray, index + 1, items.length - index);
            tempArray[index] = value;
            items = tempArray;
            size++;
        }
    }

    public void delete(int index) {
        int[] tempArray = new int[size - 1];
        System.arraycopy(items, 0, tempArray, 0, index);
        System.arraycopy(items, index + 1, tempArray, index, tempArray.length - index);
        items = tempArray;
        size--;
    }

    public void pop() {
        items = Arrays.copyOf(this.items, size - 1);
        size--;
    }

    public void replace(int index, int newValue) {
        items[index] = newValue;
    }

    public int[] getItems() {
        return items;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return Arrays.toString(items);
    }
}
