import com.epam.rd.task1.CustomArrayList;
import com.epam.rd.task2.CustomNodeList;

public class Main {
    public static void main(String[] args) {
        arrayListDemo();
        System.out.println();

        nodeListDemo();
    }

    public static void arrayListDemo() {
        CustomArrayList arrayList = new CustomArrayList();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        System.out.println(arrayList);

        arrayList.insert(10, 2);
        System.out.println(arrayList);

        arrayList.delete(1);
        System.out.println(arrayList);

        arrayList.replace(3, 99);
        System.out.println(arrayList);
    }
    public static void nodeListDemo() {
        CustomNodeList nodeList = new CustomNodeList();
        nodeList.add(1);
        nodeList.add(2);
        nodeList.add(3);
        nodeList.add(4);
        System.out.println(nodeList);

        nodeList.insert(10, 2);
        System.out.println(nodeList);

        nodeList.delete(1);
        System.out.println(nodeList);

        nodeList.replace(3, 99);
        System.out.println(nodeList);
    }
}