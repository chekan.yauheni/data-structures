# Task 1

- [x] Write a list implementation using an array. 

Implement the possibilities of
- [x] adding an element to an arbitrary place in the list, 
- [x] deleting an element, 
- [x] replacing an element.

# Task 2

- [x] Write a list implementation using unidirectional connected nodes.

Implement the possibilities of 
- [x] adding an element to an arbitrary place in the list, 
- [x] deleting an element, 
- [x] replacing an element.